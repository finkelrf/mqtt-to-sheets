import psutil
import re
import subprocess as sp
import time

def output_to_list(x): return x.decode('ascii').split('\n')[:-1]

def dataHandlerPassAll(data): return data

def onlyNumbers(data):
    found = re.search("[0-9/.]*", data)
    try:
      return float(found.group()) if found != None else None
    except ValueError:
      return None


class BaseSystemInfo():
  def __init__(self, num_reads=10, interval=1):
    self.num_reads = num_reads
    self.interval = interval

  def read(self, num_reads, interval, readFunc):
    num_reads = num_reads if num_reads is not None else self.num_reads
    interval = interval if interval is not None else self.interval
    values = []
    for i in range(0,num_reads):
      values.append(readFunc())
      time.sleep(interval)
    
    values = [value for value in values if value is not None]
    if len(values) == 0:
      return None
    return sum(values)/len(values)

class GpuInfo(BaseSystemInfo):
  def _query(self, data, dataHandlerCb=dataHandlerPassAll):
    COMMAND = f"nvidia-smi --query-gpu={data} --format=csv"
    try:
        output = output_to_list(sp.check_output(COMMAND.split(), stderr=sp.STDOUT))[1:]
    except sp.CalledProcessError as e:
        raise RuntimeError("command '{}' return with error (code {}): {}".format(
            e.cmd, e.returncode, e.output))
    return dataHandlerCb(output[0]) if len(output) > 0 else None

  def usage(self, num_reads=None, interval=None):
    f = lambda: self._query("utilization.gpu", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def fan_speed(self, num_reads=None, interval=None):
    f = lambda: self._query("fan.speed", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def memory_used(self, num_reads=None, interval=None):
    f = lambda: self._query("memory.used", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def temperature(self, num_reads=None, interval=None):
    f = lambda: self._query("temperature.gpu", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def power_draw(self, num_reads=None, interval=None):
    f = lambda: self._query("power.draw", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def memory_clock(self, num_reads=None, interval=None):
    f = lambda: self._query("clocks.current.memory", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def shaders_clock(self, num_reads=None, interval=None):
    f = lambda: self._query("clocks.current.graphics", onlyNumbers)
    return self.read(num_reads ,interval,f)

  def cuda_clock(self, num_reads=None, interval=None):
    f = lambda: self._query("clocks.current.sm", onlyNumbers)
    return self.read(num_reads ,interval,f)



class CpuInfo(BaseSystemInfo):
  def usage(self, num_reads=None, interval=None):
    f = lambda: psutil.cpu_percent()
    return self.read(num_reads ,interval,f)

  def frequency(self, num_reads=None, interval=None):
    f = lambda : psutil.cpu_freq().current
    return self.read(num_reads ,interval,f)

  def temperature(self, num_reads=None, interval=None):
    f =  lambda: psutil.sensors_temperatures()['coretemp'][0].current if 'coretemp' in psutil.sensors_temperatures().keys() else None
    return self.read(num_reads ,interval,f)

  def fan(self, num_reads=None, interval=None):
    f = lambda: psutil.sensors_fans()['asus'][0].current if "asus" in psutil.sensors_fans().keys() else None
    return self.read(num_reads ,interval,f)

class MemInfo(BaseSystemInfo):
  def usage(self, num_reads=None, interval=None):
    f = lambda: psutil.virtual_memory().percent
    return self.read(num_reads ,interval,f)
  
  def used(self, num_reads=None, interval=None):
    f = lambda: psutil.virtual_memory().used
    return self.read(num_reads ,interval,f)



if __name__ == "__main__":
  # c = CpuInfo(10,0.1)
  # print(c.temperature())
  # print(c.fan())
  # print(c.frequency())
  # print(c.usage())

  # m = MemInfo(10,0.1)
  # print(m.usage())
  # print(m.used())

  # g = GpuInfo(10,0.1)
  # print(g.usage())
  # print(g.fan_speed())
  # print(g.memory_used())
  # print(g.temperature())
  # print(g.power_draw())
  # print(g.memory_clock())
  # print(g.shaders_clock())
  # print(g.cuda_clock())
  pass
