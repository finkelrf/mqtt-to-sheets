import influxdb_client
import os
import time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

class InfluxDb():
  def __init__(self, config):
    self._config = config
    self._measurement = config.measurement
    self._point = Point(self._measurement)
    self._client = influxdb_client.InfluxDBClient(
        url=config.url, token=config.token, org=config.org)
    self._write_api = self._client.write_api(write_options=SYNCHRONOUS)

  def add_field(self, field, value):
    self._point.field(field, value)
  
  def add_field_dict(self, dict):
    for key in dict:
      self._point.field(key, dict[key])

  def write(self):
    self._write_api.write(bucket=self._config.bucket,
                          org=self._config.org, record=self._point)
    self._point = Point(self._measurement)
  
  def clear_point(self):
    self._point = Point(self._measurement)

  def query(self):
    query_api = self._client.query_api()

    query = """from(bucket: "media-center")
    |> range(start: -10m)
    |> filter(fn: (r) => r._measurement == "telemetry")"""
    tables = query_api.query(query, org=self._client.org)

    for table in tables:
      for record in table.records:
        print(record)


if __name__ == "__main__":
  import config
  db = InfluxDb(config)
  #db.add("test", 10.0)
  db.add_field_dict({'dict1':1,'dict2':2,'dict3':3})
  db.write()