from distutils.debug import DEBUG
from db import InfluxDb
from systemInfo import CpuInfo, GpuInfo, MemInfo
import config
from threading import Thread
import time
import random

DEBUG = False

def task(db, field, cb):
  value = cb()
  if DEBUG:
    print(f'{field}: {value}')
  db.add_field(field, value)

if __name__ == "__main__":
  num_reads = 10
  interval = 1
  gpu = GpuInfo(num_reads, interval)
  cpu = CpuInfo(num_reads, interval)
  mem = MemInfo(num_reads, interval)
  db = InfluxDb(config)
  threads = []
  while True:
    threads.append(Thread(target=task, args=[db, "cpu_usage", cpu.usage]))
    threads.append(Thread(target=task, args=[db, "cpu_frequency", cpu.frequency]))
    threads.append(Thread(target=task, args=[db, "cpu_temp", cpu.temperature]))
    threads.append(Thread(target=task, args=[db, "cpu_fan", cpu.fan]))

    threads.append(Thread(target=task, args=[db, "gpu_cuda_clock", gpu.cuda_clock]))
    threads.append(Thread(target=task, args=[db, "gpu_fan", gpu.fan_speed]))
    threads.append(Thread(target=task, args=[db, "gpu_memory_clock", gpu.memory_clock]))
    threads.append(Thread(target=task, args=[db, "gpu_memory_used", gpu.memory_used]))
    threads.append(Thread(target=task, args=[db, "gpu_power_draw", gpu.power_draw]))
    threads.append(Thread(target=task, args=[db, "gpu_shaders_clock", gpu.shaders_clock]))
    threads.append(Thread(target=task, args=[db, "gpu_temp", gpu.temperature]))
    threads.append(Thread(target=task, args=[db, "gpu_usage", gpu.usage]))

    threads.append(Thread(target=task, args=[db, "mem_usage", mem.usage]))
    threads.append(Thread(target=task, args=[db, "mem_used", mem.used]))

    for thread in threads:
      thread.start()

    for thread in threads:
      thread.join()

    db.write()
    threads = []
