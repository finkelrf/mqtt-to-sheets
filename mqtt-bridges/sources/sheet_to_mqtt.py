#!/usr/bin/env python3
from __future__ import print_function
import time
import paho.mqtt.client as mqtt
import os.path
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import config


class Mqtt():
  def __init__(self):
    self.mqtt_client = mqtt.Client()
    self.mqtt_client.on_connect = self.on_connect
    self.mqtt_client.on_message = self.on_message

    self.mqtt_client.connect(config.INSIDE_MQTT_HOST, config.INSIDE_MQTT_PORT, 60)

  def loop(self):
    self.mqtt_client.loop_forever()

  # The callback for when the client receives a CONNACK response from the server.
  def on_connect(self, client, userdata, flags, rc):
    print("Connected!")

  def on_message(self, client, userdata, msg):
    pass

  def publish(self, topic, message):
    self.mqtt_client.publish(topic, payload=message, retain=True)



# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

def get_sheet(id, tab, range):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    try:
        service = build('sheets', 'v4', credentials=creds)

        # Call the Sheets API
        sheet = service.spreadsheets()
        result = sheet.values().get(spreadsheetId=id,
                                    range=tab+'!'+range).execute()
        values = result.get('values', [])

        if not values:
          return None,None
        return values[0]
    except HttpError as err:
        print(err)
        return None,None


if __name__ == '__main__':
  def main():
    mqtt = Mqtt()
    temp, hum = get_sheet(config.SPREADSHEET_ID, 'living_room', 'B2:C2')
    temp = f'{round(float(temp),1)}'
    hum = f'{round(float(hum))}'
    if temp is not None:
      mqtt.publish(config.INSIDE_MQTT_TEMPERATURE_TOPIC, temp)
      print(f"living_room temp {temp}")
    if hum is not None:
      mqtt.publish(config.INSIDE_MQTT_HUMIDITY_TOPIC, hum)
      print(f"living_room hum {hum}")

    time.sleep(60*5)

  while True:
    try:
      main()
    except Exception as e:
      print("Dam, some error happened at sheet_to_mqtt. Just start it again in 10.")
      print(str(e))
      time.sleep(10)
