#!/usr/bin/env python3
import requests
from config import *
import time
import paho.mqtt.client as mqtt


def get_weather():
  api_key = "ac7447db2aaa8abd61a7253a0fbe04f7"
  base_url = "http://api.openweathermap.org/data/2.5/weather?"
  city_name = "Den Haag"
  complete_url = base_url + "appid=" + api_key + "&q=" + city_name
  response = requests.get(complete_url)
  x = response.json()
  if x["cod"] != "404":
      y = x["main"]
      current_temperature = y["temp"] - 273.15
      current_humidity = y["humidity"]
      return current_temperature, current_humidity
  else:
      return None, None


class Mqtt():
  def __init__(self):
    self.mqtt_client = mqtt.Client()
    self.mqtt_client.on_connect = self.on_connect
    self.mqtt_client.on_message = self.on_message

    self.mqtt_client.connect(WEATHER_MQTT_HOST, WEATHER_MQTT_PORT, 60)

  def loop(self):
    self.mqtt_client.loop_forever()

  # The callback for when the client receives a CONNACK response from the server.
  def on_connect(self, client, userdata, flags, rc):
    print("Connected!")

  def on_message(self, client, userdata, msg):
    pass

  def publish(self, topic, message):
    self.mqtt_client.publish(topic, payload=message, retain=True)

if __name__ == '__main__': 
  def main():
    mqtt = Mqtt()
    temp, hum = get_weather()
    print(f"{temp} {hum}")
    if temp is not None:
      mqtt.publish(WEATHER_MQTT_TEMPERATURE_TOPIC, f'{temp:.1f}')
      print("publishing temp")
    if hum is not None:
      mqtt.publish(WEATHER_MQTT_HUMIDITY_TOPIC, f'{round(hum)}')
      print("publishing hum")

    #mqtt.loop()
    print("Going to sleep")
    time.sleep(60*5)

  while True:
    try:
      main()
    except Exception as e:
      print("Dam, some error happened at weather_to_mqtt. Just start it again in 10.")
      print(str(e))
      time.sleep(10)


