#!/usr/bin/env python3
import requests
import paho.mqtt.client as mqtt
import time
from config import *

def append_to_sheet(data1, data2, data3):
    url = GSHEET_URL
    data = {"command": "insert_row", "sheet_name": "mailbox",
            "values": f"{data1},{data2},{data3}"}
    x = requests.post(url, json=data)
    print(x.text)

class MqttToSheet():
  def __init__(self):
    self.bridges = []
    print("mqtt-to-sheet starting")
    self.mqtt_client = mqtt.Client()
    self.mqtt_client.on_connect = self.on_connect
    self.mqtt_client.on_message = self.on_message

    self.mqtt_client.connect(MQTT_HOST, MQTT_PORT, 60)
    self.mqtt_client.username_pw_set(MQTT_USER, MQTT_PASSWORD)

  def loop(self):
    self.mqtt_client.loop_forever()

  def add_bridge(self, topic, callback):
    """ Callback should return zero to three values to write into google sheet. """
    self.bridges.append({
        "topic":topic,
        "callback": callback
      })
    if self.mqtt_client.is_connected:
      self.mqtt_client.subscribe(topic)

  # The callback for when the client receives a CONNACK response from the server.
  def on_connect(self, client, userdata, flags, rc):
    print("Connected!")
    for bridge in self.bridges:
      self.mqtt_client.subscribe(bridge["topic"])


  # The callback for when a PUBLISH message is received from the server.
  def on_message(self, client, userdata, msg):
      print(msg.topic+" "+str(msg.payload))
      for bridge in self.bridges:
        if msg.topic == bridge['topic']:
          data1, data2, data3 = bridge['callback'](msg.payload.decode('utf-8'))
          append_to_sheet(data1,data2,data3)

if __name__ == "__main__":
  def bat_callback(payload):
    return payload, None, None

  def got_mail_callback(payload):
    map_to_int = {
      "true":1,
      "false":0
    }
    return None,map_to_int[payload],None
  
  def main():
    bridge = MqttToSheet()
    bridge.add_bridge("/mailbox/bat_mv", bat_callback)
    bridge.add_bridge("/mailbox/got_mail", got_mail_callback)
    bridge.loop()

  while True:
    try:
      main()
    except Exception as e:
      print("Dam, some error happened at mqtt-to-sheet. Just start it again in 10.")
      print(str(e))
      time.sleep(10)
