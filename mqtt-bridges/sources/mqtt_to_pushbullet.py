#!/usr/bin/env python3
import requests
import paho.mqtt.client as mqtt
import time
from config import *
from pushbullet import Pushbullet

class MqttToPushbullet():
  def __init__(self):
    self.triggers = []
    self.pb_clients = []
    print("mqtt-to-pushbullet starting")
    self.mqtt_client = mqtt.Client()
    self.mqtt_client.on_connect = self.on_connect
    self.mqtt_client.on_message = self.on_message

    self.mqtt_client.connect(MQTT_HOST, MQTT_PORT, 60)
    self.mqtt_client.username_pw_set(MQTT_USER, MQTT_PASSWORD)

  def loop(self):
    self.mqtt_client.loop_forever()

  def add_trigger(self, topic, callback):
    """ Callback should return zero to three values to write into google sheet. """
    self.triggers.append({
        "topic": topic,
        "callback": callback
    })
    if self.mqtt_client.is_connected:
      self.mqtt_client.subscribe(topic)

  def add_pb_key(self, key):
    self.pb_clients.append(Pushbullet(key))
  
  def send_pb_message(self, msg):
    if msg is None:
      return
    for client in self.pb_clients:
      title = msg[0]
      body = msg[1] if len(msg) > 1 else ''
      client.push_note(title, body)
      print(f'{title} {body}')

  # The callback for when the client receives a CONNACK response from the server.
  def on_connect(self, client, userdata, flags, rc):
    print("Connected!")
    for trigger in self.triggers:
      self.mqtt_client.subscribe(trigger["topic"])

  # The callback for when a PUBLISH message is received from the server.
  def on_message(self, client, userdata, msg):
      print(msg.topic+" "+str(msg.payload))
      for bridge in self.triggers:
        if msg.topic == bridge['topic']:
          self.send_pb_message(bridge['callback'](msg.payload.decode('utf-8')))


if __name__ == "__main__":
  def got_mail_trigger_cb(payload):
    if payload == "1":
      return "Mailbox", "You a new got mail!"
    return None

  def main():
    mp = MqttToPushbullet()
    mp.add_pb_key(PB_KEY)
    mp.add_trigger("/mailbox/got_mail", got_mail_trigger_cb)
    mp.add_trigger("/mailbox/test", got_mail_trigger_cb)
    mp.loop()

  while True:
    try:
      main()
    except Exception as e:
      print("Dam, some error happened mqtt-to-pushbullet. Just start it again in 10.")
      print(str(e))
      time.sleep(10)
