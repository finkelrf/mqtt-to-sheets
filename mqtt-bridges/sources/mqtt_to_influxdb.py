#!/usr/bin/env python3
import requests
import paho.mqtt.client as mqtt
import time
from config import InfluxDB as config
import db

class MqttToInfluxDB():
  def __init__(self):
    self.bridges = []
    print("mqtt-to-sheet starting")
    self.db = db.InfluxDb(config)
    self.mqtt_client = mqtt.Client()
    self.mqtt_client.on_connect = self.on_connect
    self.mqtt_client.on_message = self.on_message

    self.mqtt_client.connect(config.MQTT_HOST, config.MQTT_PORT, 60)
    #self.mqtt_client.username_pw_set(config.MQTT_USER, config.MQTT_PASSWORD)

  def loop(self):
    self.mqtt_client.loop_forever()

  def add_bridge(self, topic, callback):
    """  """
    self.bridges.append({
        "topic":topic,
        "callback": callback
      })
    if self.mqtt_client.is_connected:
      self.mqtt_client.subscribe(topic)

  # The callback for when the client receives a CONNACK response from the server.
  def on_connect(self, client, userdata, flags, rc):
    print("Connected!")
    for bridge in self.bridges:
      self.mqtt_client.subscribe(bridge["topic"])


  # The callback for when a PUBLISH message is received from the server.
  def on_message(self, client, userdata, msg):
      print(msg.topic+" "+str(msg.payload))
      for bridge in self.bridges:
        data = msg.payload.decode('utf-8')
        try:
          data = float(data)
        except ValueError:
          pass
        self.db.add_field(msg.topic, data)
        self.db.write()

if __name__ == "__main__":
  def bat_callback(payload):
    pass

  def main():
    bridge = MqttToInfluxDB()
    bridge.add_bridge("#", bat_callback)
    bridge.loop()

  while True:
    try:
      main()
    except Exception as e:
      print("Dam, some error happened at mqtt-to-influxdb. Just start it again in 10.")
      print(str(e))
      time.sleep(10)
