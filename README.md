# Mqtt to Google Sheet Bridge
A simple bridge to subscribe to mqtt broker and bridge data to google sheets

This script relies on the Google Scripts, checkout the Google Script implementation at <https://github.com/StorageB/Google-Sheets-Logging>.


## How to use
Inside **source/mqtt-to-sheet.py** there is a sample application inside **if __name__ == "__main__":** you can change for your use case or **import mqtt-to-sheet** into your script if you prefer.

### Using on docker
```
# Build docker image
docker build mqtt-to-sheet/ -t mqtt-to-sheet

# Build docker media-center-data
cd media-center-data
docker build media-center-data/ -t media-center-data

# Run docker-compose
docker-compose up -d

# Check docker logs
docker logs mqtt-to-source  -f
```